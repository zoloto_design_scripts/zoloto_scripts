from tkinter.filedialog import askopenfilename
import json
import os.path
import xlrd

json_name = 'indesign_import.json'


def main():
    xls_to_import = askopenfilename(initialdir=".",
                                    title="Выберите xls файл с данными для импорта",
                                    filetypes=(("Excel files", "*.xls"), ("all files", "*.*")))

    wb = xlrd.open_workbook(xls_to_import, formatting_info=False)
    data, types = read_info_from_table(wb)
    for_json = {'data': data, 'types': types}

    file = os.path.join(os.path.dirname(xls_to_import), '..', json_name)
    json.dump(for_json, open(file, 'w'))


def read_info_from_table(wb):
    table_data = {}
    types = []

    for sn in wb.sheet_names():
        ws = wb.sheet_by_name(sn)
        sheet_data = {}
        for r in range(1, ws.nrows):
            row = ws.row_values(r)
            _id, number, var_count = int(row[0]), row[1], len(row) - 2
            jsons_row = {str(k): v for (k, v) in zip(range(var_count), row[2:])}
            jsons_row['id'] = _id
            jsons_row['number'] = number
            sheet_data[str(_id)] = jsons_row
        table_data[sn] = {}
        table_data[sn]['data'] = sheet_data
        types.append(sn)

    return (table_data, types)


if __name__ == '__main__':
    main()
