﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts/include/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts/include/checkMess.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts/include/dataManipulations.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts/include/files.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts/include/templateCases.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts/include/generationHelpers.jsx');

var data = [];
var namePrefix;
var rootDir;
var placement = '/Расстановка';

var specialChars = {
    '&br': SpecialCharacters.FORCED_LINE_BREAK,
    '&tab': '\t'
};

app.doScript(main, undefined, undefined,
    UndoModes.entireScript, "PreviewSelection");

function main() {
    var docIn = app.activeDocument;
    namePrefix = docIn.name.split('_')[0] + '_' + docIn.name.split('_')[1] + '_';
    rootDir = docIn.filePath.parent;
    var selection = selectedSplines(docIn);
    collectData(selection);

    var previewDoc = getPreviewDoc(docIn, data.shape);
    if (!previewDoc) {
        alert('Проблемы при создании файла для вывода превью.\n' +
            'Вероятно не удалось создать папки мастер-файлов.');
        return;
    }

    replacePictsNames();
    var templatesUsages = collectTemplatesUsages();
    var templatesCasesDocs = getCasesDocActualized(templatesUsages);
    var p = generatePreviews(previewDoc, templatesCasesDocs);
    closeAllTemplateDocs(templatesCasesDocs);
    alert('Превью созданы и лежат в папке\n\''
        + previewDoc.fullName + '\'');
}

function selectedSplines(docIn) {
    var buf = [];
    var selection = docIn.selection;
    for (var s = 0; s < selection.length; ++s) {
        var selected = selection[s];
        var type = selected.reflect.name;
        if (type === 'GraphicLine' || type === 'Oval'
            || type === 'Polygon' || type === 'Rectangle')
            buf.push(selected);
    }
    return buf;
}
