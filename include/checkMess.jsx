﻿function checkMess(mess) {
    var changed = false;

    if (mess.length === 0)
        return {
            text: mess,
            changed: changed
        };

    /// разбираемся с хвостовым ;
    if (mess[mess.length - 1] === ';') {                              // ; в конце сообщения
        if (mess.length === 1 || mess[mess.length - 2] === '\r')    // и либо он едиственный,
            mess = mess + '\r';             // либо один на строке и нужно поставить \r после
        else                                // не единственный и до него не перенос - убираем его
            mess = mess.slice(0, mess.length - 1);
        changed = true;
    }

    /// отлавливаем внутристрочной ;
    var lines = mess.split(';\r');
    for (var l = 0; l < lines.length; ++l) {
        if (lines[l].indexOf(';') !== -1) {
            lines[l] = lines[l].replace(';', ';\r');
            changed = true;
        }
    }
    if (changed)
        mess = lines.join(';\r');

    if (mess.indexOf('\r\n') !== -1) {
        lines = mess.split('\r\n');
        mess = lines.join('\r');
        changed = true;
    }

    return {
        text: mess,
        changed: changed
    };
}
