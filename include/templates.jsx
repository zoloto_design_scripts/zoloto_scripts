function collectTemplatesUsages() {
  var allTemplates = {};
  for (var p = 0; p < data.length; ++p) {
      for (var v = 0; v < data[p].length; ++v) {
          var field = data[p][v];

          var pr = field.indexOf('%');
          while (pr !== -1) {
              var bo = field.indexOf('(', pr);
              var bc = field.indexOf(')', pr);
              if (bo === -1 || bc === -1)
                  break;

              var name = field.slice(pr + 1, bo);
              var curTemplate = allTemplates[name];
              if (!curTemplate) {
                  curTemplate = {};
                  curTemplate.cases = 0;
              }

              var args = field.slice(bo, bc + 1);
              if (!curTemplate[args]) {
                  var nth = curTemplate.cases;
                  curTemplate[args] = nth;
                  ++curTemplate.cases;
              }
              allTemplates[name] = curTemplate;

              pr = field.indexOf('%', bc);
          }
      }
  }
  return allTemplates;
}

function getCasesDocActualized(templatesUsages) {
  // тут требуется получить словарь 'имя шаблона - файл шаблона'
  // причём файлы открыть и проверить наличие треубющихся случаев
  // при необходимости закрыть - перегенрить - открыть обновлённые
  var templatesCasesDocs = {};
  for (var templateName in templatesUsages) {
      var cases = templatesUsages[templateName];
      var templateCasesDoc = getCasesDoc(rootDir, templateName);
      if (templateCasesDoc) {
          var templateIndex = eval(templateCasesDoc.extractLabel('index'));
          var templateFile = getTemplateFile(rootDir, templateName);

          // если все случаи есть - ок, а вот если нет - файл нужно обновить
          if (checkMasterUpdates(templateCasesDoc.fullName, templateFile) || !checkCasesPresence(cases, templateIndex)) {
              templateCasesDoc.close(SaveOptions.NO);
              templateCasesDoc = generateOneTemplateCases(templateName, cases);
          }
      }
      else
          templateCasesDoc = generateOneTemplateCases(templateName, cases);
      templatesCasesDocs[templateName] = templateCasesDoc;
  }
  return templatesCasesDocs;
}

function takeTemplate(pattern, field, previewDoc, templatesCasesDocs) {
  var bo = pattern.indexOf('(');
  if (bo === -1)
      return pattern;
  var bc = pattern.indexOf(')');
  var templateName = pattern.slice(1, bo);
  var args = pattern.slice(bo, bc + 1);

  var templateCasesDoc = templatesCasesDocs[templateName];
  var templateIndex = eval(templateCasesDoc.extractLabel('index'));
  var templateID = templateIndex[args];
  var template = templateCasesDoc.textFrames.itemByID(templateID);

  var axis = ['horizontalMeasurementUnits', 'verticalMeasurementUnits'];
  for (var a = 0; a < axis.length; ++a)
      if (templateCasesDoc.viewPreferences[axis[a]] !== previewDoc.viewPreferences[axis[a]])
          templateCasesDoc.viewPreferences[axis[a]] = previewDoc.viewPreferences[axis[a]];

  var target = field.insertionPoints.lastItem().textFrames.add();
  target.properties = {
      contents: template.contents,
      geometricBounds: template.geometricBounds
  };

  var chrStyleName = template.parentStory.appliedCharacterStyle.name;
  var chrStyle = previewDoc.characterStyles.itemByName(chrStyleName);
  if (chrStyle && chrStyle.isValid)
      target.parentStory.appliedCharacterStyle = chrStyle;
  else
      alert('Не удалось восстановитьCharacterStyle \'' + chrStyleName + '\'');

  var parStyleName = template.parentStory.appliedParagraphStyle.name;
  var parStyle = previewDoc.paragraphStyles.itemByName(parStyleName);
  if (parStyle && parStyle.isValid)
      target.parentStory.appliedParagraphStyle = parStyle;
  else
      alert('Не удалось восстановить ParagraphStyle \'' + parStyleName + '\'');

  var objStyleName = template.appliedObjectStyle.name;
  var objStyle = previewDoc.objectStyles.itemByName(objStyleName);
  if (objStyle && objStyle.isValid)
      target.appliedObjectStyle = objStyle;
  else
      alert('Не удалось восстановить ObjectStyle \'' + objStyleName + '\'');

  return pattern.slice(bc + 1);
}

function closeAllTemplateDocs(templatesCasesDocs) {
  for (var templateName in templatesCasesDocs)
      templatesCasesDocs[templateName].close(SaveOptions.NO);
}